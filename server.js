//Coge la configuracion por defecto de dotenv con el config
require("dotenv").config();

const express = require('express');
const app = express();


var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}

const port = process.env.PORT || 3000;
app.use(express.json());
app.use(enableCORS);


const UserControllers = require('./Controllers/UserControllers');
const AuthController = require('./Controllers/AuthController');
const CuentasControllers = require('./Controllers/CuentasControllers');

app.listen(port);
console.log("API escuchando en el puerto cambio2 " + port);

app.get('/apitchu/v1/hello',
 function(req, res)  {
    console.log("GET /apitchu/v1/hello");
   res.send({"msg" :"Hola desde API TechU"});
  }
)

app.get('/apitechu/v1/users',UserControllers.getUsersV1);
app.get('/apitechu/v2/users',UserControllers.getUsersV2);
app.get('/apitechu/v2/users/:id', UserControllers.getUserByIdV2);
app.get('/apitechu/v2/cuentas/:idUsuario', CuentasControllers.getCuentasById);
app.post('/apitechu/v1/users',UserControllers.createUserV1);
app.post('/apitechu/v2/users',UserControllers.createUserV2);
app.delete('/apitechu/v1/users',UserControllers.deleteUserV1);


app.post('/apitechu/v1/login', AuthController.userlogin);
app.post('/apitechu/v2/login', AuthController.userloginV2);
app.post('/apitechu/v1/logout/:id', AuthController.userlogout);
app.post('/apitechu/v2/logout/:id', AuthController.userlogoutV2);



app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req, res) {
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

  }

)
